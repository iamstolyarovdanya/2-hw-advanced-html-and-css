const headerMenu = document.querySelector(`.header__menu`);
const btn = document.querySelector(`.header__btn`);
const photoClick = document.getElementById(`photo_click`);

btn.addEventListener(`click`, (event)=>{
headerMenu.classList.toggle(`view`);
if (!headerMenu.classList.contains(`view`)) {
    photoClick.setAttribute(`src` , `./images/svg_exit.svg`)
}else{
    photoClick.setAttribute(`src` , `./images/svg_click.svg`)
}
})